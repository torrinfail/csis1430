#!/bin/sh
(command -v git && command -v ssh) || exit 1
GITHOST=incawasi.ddns.net
ssh-keygen
scp ~/.ssh/id_rsa.pub $1:amsk || exit 1
ssh $1 -t "scp amsk $GITHOST:amsk && ssh $GITHOST -t \"print '\\n' >> .ssh/authorized_keys && cat amsk >> .ssh/authorized_keys\""
[ "$2" = "-k" ] && exit
git clone $GITHOST:/gitrepos/dotfiles
git clone $GITHOST:/gitrepos/bin
mkdir ~/repos
cd ~/repos || exit
git clone $GITHOST:/gitrepos/dwm
git clone $GITHOST:/gitrepos/dmenu
git clone $GITHOST:/gitrepos/st
git clone $GITHOST:/gitrepos/SF-Mono-Font
git clone $GITHOST:/gitrepos/PassGet
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
cd ~/dotfiles || exit
./create_links
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
