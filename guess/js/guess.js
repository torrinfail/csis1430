var img = document.getElementsByTagName("IMG")[0];
var button = document.getElementsByTagName("BUTTON")[0];
var randNum = Math.ceil(Math.random() * 99);
var gameText = document.getElementById("gameTxt");
function getGuess()
{
	var guess = parseInt(prompt("Guess a number from 1-99."));
	if (isNaN(guess))
	{
		gameText.innerHTML = "Not a Valid Number, Try Again";
		return;
	}
	if (guess < randNum)
	{
		gameText.innerHTML = "Too Low, Try Again!";
		button.innerHTML = "Try Again";
		img.src = "img/downarrow.png";
		return;
	}
	if (guess > randNum)
	{
		gameText.innerHTML = "Too High, Try Again!";
		button.innerHTML = "Try Again";
		img.src = "img/uparrow.png";
		return;
	}
	gameText.innerHTML = "That's Correct!!!"
	button.innerHTML = "Play Again";
	button.onclick = restart;
	img.src = "img/correct.png";

}

function restart()
{
	location.reload();
}
