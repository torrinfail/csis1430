#!/bin/bash

#=============================================================================
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# You may have to adjust these settings:

XCOMMAND="/usr/bin/nice -n -20 /usr/bin/Xephyr"
XEVDEVSERVERCOMMAND="/usr/local/sbin/startsched 20 /usr/local/sbin/xevdevserver -fork"

# The rest of the script should probably be left alone
#=============================================================================

echo "XevdevScript.sh v1.0"
echo "Processing command line arguments"

UNDERLYINGDISPLAY="not set"
UNDERLYINGAUTH="not set"
OTHEROPTIONS=""
XEVDEVCONNECTIONS=""

UNDERLYINGAUTH="$XAUTHORITY"
UNDERLYINGDISPLAY="$DISPLAY"

while [ $# != 0 ] ; do
	if [ "$1" = "-auth" ] ; then
		UNDERLYINGAUTH="$2"
		shift
	elif [ "$1" = "-xevdev" ] ; then
		echo
		echo "Examining connection $2"
		echo
		
		XGLDISPLAY=`echo "$2"|cut -f 1 -d ","`
		KEYBOARDDEVICE=`echo "$2"|cut -f 2 -d ","`
		MOUSEDEVICE=`echo "$2"|cut -f 3 -d ","`

		echo "XGLDISPLAY=$XGLDISPLAY"
		echo "KEYBOARDDEVICE=$KEYBOARDDEVICE"
		echo "MOUSEDEVICE=$MOUSEDEVICE"

		tmp=`echo "$XGLDISPLAY" | grep -e "\(^:[0-9]\+$\)\|\(^:[0-9]\+\.[0-9]\+$\)"`
		if [ "tmp$tmp" = "tmp" ] ; then
			echo "Detected invalid Xgl display"
			exit -1
		fi

		# Check if the device names need translating

		echo "Checking keyboard device name"

		tmp=`echo "$KEYBOARDDEVICE" | grep -e "^/dev/"`
		if [ "tmp$tmp" = "tmp" ] ; then # look in /proc/bus/input/devices
			echo "Doesn't start with /dev, checking /proc/bus/input/devices"
			IFS=$'\t'
			for i in `cat /proc/bus/input/devices |grep -A 5 -e "^I:"|sed -e "s/^--/\t/"` ;do 
				tmp=`echo "$i" | grep -e "^H: " | grep "kbd"`
				if [ "tmp$tmp" != "tmp" ] ; then 
					tmp=`echo "$i" | grep "$KEYBOARDDEVICE"`
					if [ "tmp$tmp" != "tmp" ] ; then 
					#	echo "Found keyboard device:"
					#	echo "$i"
						KEYBOARDDEVICE=/dev/input/`echo "$i" | grep -e "^H: " | grep --only-matching -e "event[0-9]\+"`
					#	echo "Device stored"
					fi
				fi
			done
			IFS=$' \t\n'
		else
			echo "Starts with /dev, passing it directly to xevdevserver"
		fi

		echo "KEYBOARDDEVICE=$KEYBOARDDEVICE"

		tmp=`echo "$KEYBOARDDEVICE" | grep -e "^/dev/"`
		if [ "tmp$tmp" = "tmp" ] ; then
			echo "Detected invalid keyboard specification"
			exit -1
		fi

		echo "Checking mouse device name"

		tmp=`echo "$MOUSEDEVICE" | grep -e "^/dev/"`
		if [ "tmp$tmp" = "tmp" ] ; then # look in /proc/bus/input/devices
			echo "Doesn't start with /dev, checking /proc/bus/input/devices"
			IFS=$'\t'
			for i in `cat /proc/bus/input/devices |grep -A 5 -e "^I:"|sed -e "s/^--/\t/"` ;do 
				tmp=`echo "$i" | grep -e "^H: " | grep "mouse"`
				if [ "tmp$tmp" != "tmp" ] ; then 
					tmp=`echo "$i" | grep "$MOUSEDEVICE"`
					if [ "tmp$tmp" != "tmp" ] ; then 
					#	echo "Found mouse device:"
					#	echo "$i"
						MOUSEDEVICE=/dev/input/`echo "$i" | grep -e "^H: " | grep --only-matching -e "event[0-9]\+"`
					#	echo "Device stored"
					fi
				fi
			done
			IFS=$' \t\n'
		else
			echo "Starts with /dev, passing it directly to xevdevserver"
		fi

		echo "MOUSEDEVICE=$MOUSEDEVICE"

		tmp=`echo $MOUSEDEVICE | grep -e "^/dev/"`
		if [ "tmp$tmp" = "tmp" ] ; then
			echo "Detected invalid mouse specification"
			exit -1
		fi

		echo "Mouse and keyboard device detected, continuing"
		echo

		XEVDEVCONNECTIONS="$XEVDEVCONNECTIONS $XGLDISPLAY,$KEYBOARDDEVICE,$MOUSEDEVICE"
		
		shift
	else
		dispcheck=`echo "$1"|grep -e "\(^:[0-9]\+$\)\|\(^:[0-9]\+\.[0-9]\+$\)"`
		if [ "bla$dispcheck" != "bla" ] ; then 
			UNDERLYINGDISPLAY="$1"
		else
			OTHEROPTIONS="$OTHEROPTIONS $1"
		fi
	fi
	
	shift
done

echo "Detected settings:"
echo "UNDERLYINGDISPLAY=$UNDERLYINGDISPLAY"
echo "UNDERLYINGAUTH=$UNDERLYINGAUTH"
echo "OTHEROPTIONS=$OTHEROPTIONS"
echo "XEVDEVCONNECTIONS=$XEVDEVCONNECTIONS"
echo
	
tmp=`echo "$UNDERLYINGDISPLAY" | grep -e "\(^:[0-9]\+$\)\|\(^:[0-9]\+\.[0-9]\+$\)"`
if [ "tmp$tmp" = "tmp" ] ; then
	echo "Detected invalid underlying display"
	exit -1
fi

if ! [ -e "$UNDERLYINGAUTH" ] ; then
	echo "Couldn't detect X authority file"
	exit -1
fi

echo
echo "Starting xevdevserver2 instances"
echo 

for i in `echo "$XEVDEVCONNECTIONS"` ; do
	XGLDISPLAY=`echo "$i"|cut -f 1 -d ","`
	KEYBOARDDEVICE=`echo "$i"|cut -f 2 -d ","`
	MOUSEDEVICE=`echo "$i"|cut -f 3 -d ","`

	underlyingdisplaynum=`echo "$UNDERLYINGDISPLAY"|cut -f 2 -d :|cut -f 1 -d .`
	xgldisplaynum=`echo "$XGLDISPLAY"|cut -f 2 -d :|cut -f 1 -d .`
	authdir=`dirname $UNDERLYINGAUTH`
	authname=`basename $UNDERLYINGAUTH`
	xglauthname=`echo $authname|sed -e "s/$underlyingdisplaynum/$xgldisplaynum/"`
	XGLAUTH="$authdir/$xglauthname"

	echo "$XGLAUTH $XEVDEVSERVERCOMMAND -d $XGLDISPLAY -k $KEYBOARDDEVICE -m $MOUSEDEVICE"
	( ( XAUTHORITY=$XGLAUTH $XEVDEVSERVERCOMMAND -d $XGLDISPLAY -k $KEYBOARDDEVICE -m $MOUSEDEVICE ) & ) &
done

echo
echo "Starting X server"
echo

exec $XCOMMAND $OTHEROPTIONS -auth $UNDERLYINGAUTH $UNDERLYINGDISPLAY

