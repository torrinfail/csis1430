class Tile 
{
	team = "-";
}
class Player 
{
	name;
	team;
	mask = 0;
	constructor(name, team)
	{
		this.name = name;
		this.team = team;
	}
}
const gameInitial = document.getElementById("game_window").innerHTML;
var playerx = new Player(prompt("Enter Name For First Player \"X\""), "X");
var playero = new Player(prompt("Enter Name For First Player \"O\""), "O");
var currentPlayer;
var turnCount;
var won;
var board = new Array(9);
var winners = [448,56,7,292,146,73,273,84]; 
setup();

function setup()
{
	document.getElementById("game_window").innerHTML = gameInitial;
	for(i = 0; i < board.length; i++)
		board[i] = new Tile();
	playero.mask = 0;
	playerx.mask = 0;
	turnCount = 0;
	won = false;
	currentPlayer = null;
	nextTurn();
}
function setTile(num)
{
	if (board[num].team != "-" || won)
		return board[num].team;
	currentPlayer.mask |= 1 << num;
	board[num].team = currentPlayer.team;
	if (!checkWin())
		nextTurn();
	else
		win(currentPlayer);
	return board[num].team;
}

function checkWin()
{
	if (won)
		return true;
	won = false;
	for (i = 0; i < winners.length; i++)
		if ((currentPlayer.mask & winners[i]) == winners[i])
		{
			won = true;
			break;
		}
	return won;
}

function restart()
{
	setup();
}

function nextTurn()
{
	if (++turnCount > 9)
	{
		win(null);
		return;
	}
	currentPlayer = currentPlayer == playerx ? playero:playerx;
	document.getElementById("info").innerHTML = currentPlayer.name + "'s turn.";
}

function win(player)
{
	if (player == null)
		document.getElementById("info").innerHTML = "TIE!";
	else
		document.getElementById("info").innerHTML = player.name + " WINS!";
	document.getElementById("playagain").style = "";
	document.getElementById("playagain").onclick = restart;
}
